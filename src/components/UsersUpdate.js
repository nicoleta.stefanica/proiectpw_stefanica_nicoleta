import React, { useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { DB } from "../firebase";
import { doc, getDoc, setDoc } from "firebase/firestore";
import { getAuth } from "firebase/auth";


export function UserUpdate() {
    const navigate = useNavigate();
    const { id } = useParams();
    const [updateEffect, setUpdateEffect] = useState(false);
    const [details, setDetails] = useState({
        firstName: '',
        lastName: '',
        phoneNumber: '',
        adress: '',
        email: ''
    });

    function getUserID() {
        const auth = getAuth();
        const user = auth.currentUser;
        if (user !== null) {
            return user.email;
        }
    }

    const email = getUserID()

    // inainte puneam asta in useEffect si dependinta ,[id]
    const fetchData = async () => {
        const docRef = doc(DB, 'users', id);
        const docSnap = await getDoc(docRef);

        if (docSnap.exists()) {
            const data = docSnap.data();
            setDetails(data);
        } else {
            console.log('data not found.');
        }
    };

    const handleGoBack = () => {
        if (email === 'admin@gmail.com')
            navigate('/users')
        else
            navigate('/account')
    }

    const handleUpdate = async () => {
        const docRef = doc(DB, 'users', id);
        try {

            await setDoc(docRef, details);
            setUpdateEffect(true)
            alert("Data updated successfully")
            //navigate('/edit');
        } catch (error) {
            console.error('Error updating data:', error);
        }
    };

    useEffect(() => {
        fetchData()
        setUpdateEffect(false)
        console.log('ceva')
    }, [updateEffect]);

    return (
        <div>

            <div className='add'>
                <div className='form-container'>
                    <br />
                    <h2>EDIT USER DATA</h2>
                    <hr />
                    <div className='form-group'>
                        <label htmlFor="product-name">First Name</label>
                        <input type="text" className='form-control' placeholder={details.firstName}
                            onChange={(e) => setDetails({ ...details, firstName: e.target.value })} />
                        <br />
                        <label htmlFor="product-name">Last Name</label>
                        <input type="text" className='form-control' placeholder={details.lastName}
                            onChange={(e) => setDetails({ ...details, lastName: e.target.value })} />
                        <br />
                        <label htmlFor="product-price">Phone Number</label>
                        <input type="text" className='form-control' placeholder={details.phoneNumber}
                            onChange={(e) => setDetails({ ...details, phoneNumber: e.target.value })} />
                        <br />
                        <label htmlFor="product-price">Address</label>
                        <input type="text" className='form-control' placeholder={details.adress}
                            onChange={(e) => setDetails({ ...details, adress: e.target.value })} />
                        <br />
                        <label htmlFor="product-price">Email</label>
                        <input type="text" className='form-control' placeholder={details.email}
                            onChange={(e) => setDetails({ ...details, email: e.target.value })} />
                        <br />

                        <div className='butoane-update'>
                            <button type="button" class="btn btn-primary" onClick={handleUpdate}>Update data</button>
                            <button type="button" class="btn btn-primary" onClick={handleGoBack}>Go Back</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
