import React from 'react'
import IndividualCartProduct from './individualCartProduct'

export const CartProducts = ({cartProducts, cartProductIncrease, cartProductDecrease, cartProductDelete}) => {
  return cartProducts.map((cartProduct) => (
    <IndividualCartProduct 
    key={cartProduct.productTitle} cartProduct={cartProduct}
    cartProductIncrease={cartProductIncrease}
    cartProductDecrease={cartProductDecrease}
    cartProductDelete={cartProductDelete}>
    </IndividualCartProduct>
  ))
}

export default CartProducts
