import React, { useEffect, useState } from 'react'
import { Navbar } from '../navbar/navbar'
import { DB } from "../../firebase";
import { getAuth } from "firebase/auth";
import { CartProducts } from './CartProducts'
import { collection, getDocs } from 'firebase/firestore';
import { doc, updateDoc, deleteDoc } from "firebase/firestore";
import './cartProduct.css'

export const Cart = () => {

    const [cartProducts, setCartProducts] = useState([])
    const [incrEffect, setIncrEffect] = useState(false);
    const [decrEffect, setDecrEffect] = useState(false);
    const [deleteEffect, setDeleteEffect] = useState(false);
    const [placeOrderEffect, setPlaceOrderEffect] = useState(false);

    function getUserID() {
        const auth = getAuth();
        const user = auth.currentUser;
        if (user !== null) {

            return user.uid;
        }
    }

    const uid = getUserID()

    const getData = async () => {
        const valRef = collection(DB, `Cart${uid}`)
        const dataDb = await getDocs(valRef)
        const allData = dataDb.docs.map(val => ({ ...val.data(), id: val.id }))
        setCartProducts(allData)
    }

    let Product

    

    const cartProductIncrease = async (cartProduct) => {
        // console.log(cartProduct)
        Product = cartProduct
        Product.qty = Product.qty + 1
        Product.TotalProductPrice = Product.qty * Product.productPrice

        if (uid) {
            const ref = doc(DB, `Cart${uid}`, Product.productTitle);
            await updateDoc(ref, Product)
            setIncrEffect(true)
        }
        else {
            console.log('User is not logged in')
        }
    }

    const cartProductDecrease = async (cartProduct) => {
        Product = cartProduct

        if (Product.qty > 1) {
            Product.qty = Product.qty - 1
            Product.TotalProductPrice = Product.qty * Product.productPrice
        }

        if (uid) {
            const ref = doc(DB, `Cart${uid}`, Product.productTitle);
            await updateDoc(ref, Product)
            setDecrEffect(true)
        }
        else {
            console.log('User is not logged in')
        }
    }

    const placeOrder = async () => {
        const valRef = collection(DB, `Cart${uid}`)
        const dataDb = await getDocs(valRef)
        const allData = dataDb.docs.map(val => ({ ...val.data(), id: val.id }))

        try {
            for (const documentId of allData) {
                await deleteDoc(doc(DB, `Cart${uid}`, documentId.productTitle));
                setPlaceOrderEffect(true);
            }
          } catch (error) {
            console.error('Error deleting documents:', error);
          }finally{
            alert('Your order has been placed! :)')
          }
    }

    const cartProductDelete = async (cartProduct) => {
        Product = cartProduct

        if (uid) {
            await deleteDoc(doc(DB, `Cart${uid}`, Product.productTitle));
            setDeleteEffect(true);
        }
        else {
            console.log('User is not logged in')
        }
    }

    // Number of products:
    const qty = cartProducts.map(cartProduct => {
        return cartProduct.qty
    })

    const reduceQtyOf = (accumulator, currentValue) => accumulator + currentValue

    const totalQty = qty.reduce(reduceQtyOf, 0)

    // Pret total:
    const price = cartProducts.map(cartProduct => {
        return cartProduct.TotalProductPrice
    })

    const reducePriceOf = (accumulator, currentValue) => accumulator + currentValue

    const totalPrice = price.reduce(reducePriceOf, 0)


    useEffect(() => {
        getData();
        console.log('ceva')
        setDeleteEffect(false);
        setIncrEffect(false)
        setDecrEffect(false)
        setPlaceOrderEffect(false)
    }, [deleteEffect, decrEffect, incrEffect, placeOrderEffect]);

    return (
        <div>
            <Navbar />
            <br></br>
            {cartProducts.length > 0 && (
                <div>
                    <div className='container-fluid'>
                        <h1 className='text-center'>Cart</h1>
                        <div className='products-box'>
                            <CartProducts cartProducts={cartProducts}
                                cartProductIncrease={cartProductIncrease}
                                cartProductDecrease={cartProductDecrease}
                                cartProductDelete={cartProductDelete}
                            />
                        </div>
                    </div>

                    <div className='summary-box'>
                        <h5>Cart Summary</h5> <br></br>
                        <div>
                            Total number of products: <span>{totalQty}</span>
                        </div>
                        <div>
                            Total price to pay: $<span>{totalPrice}</span>
                        </div>
                        <button type="button"  class="btn btn-outline-primary" onClick={placeOrder}>PLACE ORDER</button>
                    </div>
                </div>
            )}
            {
                cartProducts.length < 1 && (
                    <div>
                        <div className='mesaj-empty'>Your cart is empty..</div>
                    </div>
                )
            }
        </div>
    )
}

export default Cart
