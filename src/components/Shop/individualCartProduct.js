import React from 'react'
import { Icon } from 'react-icons-kit'
import { plus } from 'react-icons-kit/feather/plus'
import { minus } from 'react-icons-kit/feather/minus'
import './cartProduct.css'

export const IndividualCartProduct = ({ cartProduct, cartProductIncrease,
    cartProductDecrease, cartProductDelete }) => {

    const handleCartProductIncrease = () => {
        cartProductIncrease(cartProduct)
    }

    const handleCartProductDecrease = () => {
        cartProductDecrease(cartProduct)
    }

    const handleCartProductDelete = () => {
        cartProductDelete(cartProduct)
    }

    return (
        <div>
            <div className='product-cart' key={cartProduct.ProductID}>
                <div className='image-cart'>
                    <img
                        src={cartProduct.imgUrl}
                        alt="not found"
                        style={{ width: '140px', height: '200px' }}
                    />
                </div>
                <div className='title-cart'>
                    {cartProduct.productTitle}
                </div>
                <div className='author-cart'>
                    {cartProduct.productAuthor}
                </div>

                <div className='cantitate'>
                    <div className='action-btns minus'>
                        <Icon icon={minus} size={20} onClick={handleCartProductDecrease} />
                    </div>
                    <div>{cartProduct.qty}</div>
                    <div className='action-btns plus'>
                        <Icon icon={plus} size={20} onClick={handleCartProductIncrease} />
                    </div>
                </div>

                <div>Total: ${cartProduct.TotalProductPrice}</div>
                <button type="button" class="btn btn-outline-danger" onClick={handleCartProductDelete}>delete</button>
            </div>
        </div>

    )
}

export default IndividualCartProduct
