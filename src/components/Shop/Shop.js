import React, {useEffect, useState } from 'react'
import { Navbar } from '../navbar/navbar'
import { Footer } from '../footer/Footer'
import { DB } from "../../firebase";
import { collection, getDocs,  doc, setDoc } from 'firebase/firestore';
import './shop.css'
import { getAuth } from "firebase/auth";


export function Shop() {

  const [data, setData] = useState([])

  function getUserID() {
    const auth = getAuth();
    const user = auth.currentUser;
    if (user !== null) {

      return user.uid;
    }
  }

  const uid = getUserID()

  const handleAddToCart = async (product) => {
    if (uid) {
      try {
        product['qty'] = 1
        product['TotalProductPrice'] = product.qty * product.productPrice
        const cartCollection = collection(DB, `Cart${uid}`);
        const productDoc = doc(cartCollection, product.productTitle);

        await setDoc(productDoc, product);

        alert("Product added to your cart!")
        console.log('Product added to cart');
      } catch (error) {
        console.error('Error adding product to cart:', error);
      }
    } else {
      alert('Log in to your account to add products to cart!')
    }
  };

  const getData = async () => {
    const valRef = collection(DB, 'Products')
    const dataDb = await getDocs(valRef)
    const allData = dataDb.docs.map(val => ({ ...val.data(), id: val.id }))
    setData(allData)
    console.log(dataDb)
  }

  useEffect(() => {
    getData();
    console.log('ceva shop')
  }, []);

  return (
    <>
      <Navbar />
      {data.length !== 0 && <div><h1 className='title-shop'>Books</h1></div>}
      <div className='products-container2'>
        {data.map(product => (
          <div className='product-card2' key={product.ProductID}>
            <div className='product-img'>
              <img
                src={product.imgUrl}
                alt="not found"
                style={{ maxWidth: '200px', height: '300px' }}
              />
            </div>
            <div className='product-title' >
              {product.productTitle}
            </div>
            <div className='product-author'>
              {product.productAuthor}
            </div>
            <div className='product-price'>
              Price: ${product.productPrice}
            </div>
            <button type="button" class="btn btn-success" onClick={() => handleAddToCart(product)}>ADD TO CART</button>
          </div>
        ))}
      </div>
      <Footer className='footer-shop'/>
    </>

  )
}

export default Shop