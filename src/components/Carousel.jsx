import React from 'react'
import Slider from "react-slick";
import { BiSolidCart } from "react-icons/bi";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import BrilliantFriend from './images/brilliant_friend.jpg'
import SecretHistory from './images/secrethistory.jpg'
import Metamorfoza from './images/metamorfoza.jpg'
import Perfume from './images/perfume.jpg'
import Atomic from './images/atomic.jpeg'
import TheStranger from './images/thestranger.jpg'

function Arrow(props) {
  const { className, style, onClick } = props;
  return (
    <div
      className={className}
      style={{ ...style, display: "block", background: "#0f176a" }}
      onClick={onClick}
    />
  );
}


export function Carousel() {

  var settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 1,
    nextArrow: <Arrow />,
    prevArrow: <Arrow />,
    initialSlide: 0,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
        }
      }
    ]
  };

  return (
    <div>
      <Slider {...settings}>
      <div class="product-container">
            <div class="product-card">
              <img src={BrilliantFriend} class="image" alt="brilliantFriend" />
            </div>

            <div class="product-info">
              <h3 class="name">my brilliant friend</h3>
              <p class="author">Elena Ferrante</p>
              {/* <div class="price-and-buy">
                <span class="price">15$</span>
                <BiSolidCart className='buy-btn'/>
              </div> */}
            </div>
          </div>

          <div class="product-container">
            <div class="product-card">
              <img class="image" src={TheStranger} alt="imagine" />
            </div>

            <div class="product-info">
              <h3 class="name">the stranger</h3>
              <p class="author">albert camus</p>
              {/* <div class="price-and-buy">
                <span class="price">17$</span>
                <BiSolidCart className='buy-btn'/>
              </div> */}
            </div>
          </div>

          <div class="product-container">
            <div class="product-card">
              <img class="image" src={SecretHistory} alt="imagine" />
            </div>

            <div class="product-info">
              <h3 class="name">the secret history</h3>
              <p class="author">Donna Tart</p>
              {/* <div class="price-and-buy">
                <span class="price">20$</span>
                <BiSolidCart className='buy-btn'/>
              </div> */}
            </div>
          </div>

          <div class="product-container">
            <div class="product-card">
              <img class="image" src={Metamorfoza} alt="imagine" />
            </div>

            <div class="product-info">
              <h3 class="name">the metamorphosis</h3>
              <p class="author">Franz Kafka</p>
              {/* <div class="price-and-buy">
                <span class="price">10$</span>
                <BiSolidCart className='buy-btn'/>
              </div> */}
            </div>
          </div>

          <div class="product-container">
            <div class="product-card">
              <img class="image" src={Perfume} alt="imagine" />
            </div>

            <div class="product-info">
              <h3 class="name">perfume</h3>
              <p class="author">patrick suskind</p>
              {/* <div class="price-and-buy">
                <span class="price">12$</span>
                <BiSolidCart className='buy-btn'/>
              </div> */}
            </div>
          </div>

          <div class="product-container">
            <div class="product-card">
              <img class="image" src={Atomic} alt="imagine" />
            </div>

            <div class="product-info">
              <h3 class="name">atomic habits</h3>
              <p class="author">james clear</p>
              {/* <div class="price-and-buy">
                <span class="price">23$</span>
                <BiSolidCart className='buy-btn'/>
              </div> */}
            </div>
          </div>


      </Slider>
    </div>
  )
}

export default Carousel
