import "./Login.css"

import React, { useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { UserAuth } from '../context/AuthContext';
import { addDoc, collection } from "firebase/firestore";
import {DB} from '../firebase'

export const Signup = () => {
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [error, setError] = useState('')
  const { createUser } = UserAuth()
  const navigate = useNavigate()

  const [firstName, setFirstName] = useState('')
  const [lastName, setLastName] = useState('')
  const [phoneNumber, setPhoneNumber] = useState('')
  const [adress, setAdress] = useState('')

  const handleSubmit = async (e) => {
    e.preventDefault();
    setError('');
    try {
      await createUser(email, password);
      const valRef = collection(DB, 'users')

      await addDoc(valRef, {
        firstName: firstName,
        lastName: lastName,
        phoneNumber: phoneNumber,
        email: email,
        adress: adress
      })
      navigate('/home')
    } catch (e) {
      setError(e.message);
      console.log(e.message);
    }
  }

  return (
    <div class="signup-container">
      <div class="login">
        
        <form onSubmit={handleSubmit} >
          <div class="line">
            <div><label>First Name</label></div>
            <input className="signup-line" type="text" onChange={(e) => setFirstName(e.target.value)} required />
          </div>

          <div class="line">
            <div><label>Last Name</label></div>
            <input type="text" onChange={(e) => setLastName(e.target.value)} required />
          </div>

          <div class="line">
            <div><label>Phone Number</label></div>
            <input type="text" onChange={(e) => setPhoneNumber(e.target.value)} required />
          </div>

          <div class="line">
            <div><label>Address</label></div>
            <input type="text" onChange={(e) => setAdress(e.target.value)} required />
          </div>

          <div class="line">
            <div><label>Email Address</label></div>
            <input onChange={(e) => setEmail(e.target.value)} type="email" required />
          </div>

          <div class="line">
            <div><label>Password</label></div>
            <input onChange={(e) => setPassword(e.target.value)} type="password" required />
          </div>

          <button className='buton-sign'> Sign Up </button>

          <div class="new_account">
            Already have an account? <Link to="/"> Sign In</Link>
          </div>
        </form>
      </div>
    </div>
  )
}