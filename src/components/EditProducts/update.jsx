import React, { useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { DB } from "../../firebase";
import { doc, getDoc, setDoc } from "firebase/firestore";


export function Update() {
    const navigate = useNavigate();
    const { id } = useParams();
    const [updateEffect, setUpdateEffect] = useState(false);
    const [product, setProduct] = useState({
        imgUrl: '',
        productTitle: '',
        productAuthor: '',
        productPrice: '',
    });

    // inainte puneam asta in useEffect si dependinta ,[id]
    const fetchData = async () => {
        const docRef = doc(DB, 'Products', id);
        const docSnap = await getDoc(docRef);

        if (docSnap.exists()) {
            const data = docSnap.data();
            setProduct(data);
        } else {
            console.log('Product not found.');
        }
    };

    const handleGoBack = () => {
        navigate('/edit')
    }

    const handleUpdate = async () => {
        const docRef = doc(DB, 'Products', id);
        try {

            await setDoc(docRef, product);
            setUpdateEffect(true)
            alert("Product updated successfully")
            //navigate('/edit');
        } catch (error) {
            console.error('Error updating product:', error);
        }
    };

    useEffect(() => {
        fetchData()
        setUpdateEffect(false)
        console.log('ceva')
    }, [updateEffect]);

    return (
        <div>
            <div className='add'>
                <div className='form-container'>
                    <br />
                    <h2>EDIT PRODUCTS</h2>
                    <hr />
                    <div className='form-group'>
                        <label htmlFor="product-name">Title</label>
                        <input type="text" className='form-control' placeholder={product.productTitle}
                            onChange={(e) => setProduct({ ...product, productTitle: e.target.value })} />
                        <br />
                        <label htmlFor="product-name">Author</label>
                        <input type="text" className='form-control' placeholder={product.productAuthor}
                            onChange={(e) => setProduct({ ...product, productAuthor: e.target.value })} />
                        <br />
                        <label htmlFor="product-price">Price</label>
                        <input type="number" className='form-control' placeholder={product.productPrice}
                            onChange={(e) => setProduct({ ...product, productPrice: e.target.value })} />
                        <br />

                        <div className='butoane-update'>
                            <button type="button" class="btn btn-primary" onClick={handleUpdate}>Update Product</button>
                            <button type="button" class="btn btn-primary" onClick={handleGoBack}>Go Back</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
