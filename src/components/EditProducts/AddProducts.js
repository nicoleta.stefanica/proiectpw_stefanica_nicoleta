import React, { useState } from "react";
import { imgDB, txtDB } from "../../firebase";
import { v4  } from 'uuid';
import { getDownloadURL, ref, uploadBytes } from "firebase/storage";
import { addDoc, collection } from "firebase/firestore";
import { Navbar2 } from '../navbar/navbarAdmin'
import "./add.css"

function AddProducts() {
  const [productTitle, setProductTitle] = useState('')
  const [productAuthor, setProductAuthor] = useState('')
  const [selectedFile, setSelectedFile] = useState()
  const [productPrice, setProductPrice] = useState(0)

  const handleUpload = async (file) => {
    if (file) {
      try {
        const storageRef = ref(imgDB, `Imgs/${v4()}`);
        const snapshot = await uploadBytes(storageRef, file);
        const downloadURL = await getDownloadURL(snapshot.ref);

        return downloadURL
      } catch (error) {
        console.error("Eroare la încărcarea imaginii: ", error);
      }
    }
  }

  const handleClick = async () => {
    const valRef = collection(txtDB, 'Products')
  
    const file = await handleUpload(selectedFile) 
    await addDoc(valRef, {
      productTitle: productTitle,
      productAuthor: productAuthor,
      productPrice: productPrice,
      imgUrl: file
    })
    alert("Product added successfully")
  }

  return (
    <div>
      <Navbar2 />
      <div className='add'>
        <div className='form-container'>
          <br />
          <h2>ADD PRODUCTS</h2>
          <hr />
          <div className='form-group'>
            <label htmlFor="product-name">Title</label>
            <input type="text" className='form-control' required
              onChange={(e) => setProductTitle(e.target.value)} value={productTitle} />
            <br />
            <label htmlFor="product-name">Author</label>
            <input type="text" className='form-control' required
              onChange={(e) => setProductAuthor(e.target.value)} value={productAuthor} />
            <br />
            <label htmlFor="product-price">Price</label>
            <input type="number" className='form-control' required
              onChange={(e) => setProductPrice(e.target.value)} value={productPrice} />
            <br />
            <label htmlFor="product-img">Product Image</label>
            <input type="file" className='form-control' id="file" required
              onChange={(e) => setSelectedFile(e.target.files[0])} />
            <br />
            <button className='btn btn-success btn-md mybtn' onClick={handleClick}>ADD</button>
          </div>
        </div>
      </div>
    </div>
  )
}

export default AddProducts
