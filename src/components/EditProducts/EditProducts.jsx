import React, { useEffect, useState } from 'react'
import { Navbar2 } from '../navbar/navbarAdmin'
import { DB } from "../../firebase";
import { useNavigate } from "react-router-dom"
import { collection, getDocs } from "firebase/firestore";
import { deleteDoc, doc } from "firebase/firestore";
import './edit.css'

export function EditProducts() {

    const [data, setData] = useState([])
    const navigate = useNavigate()

    const getData = async () => {
        const valRef = collection(DB, 'Products')
        const dataDb = await getDocs(valRef)
        const allData = dataDb.docs.map(val => ({ ...val.data(), id: val.id }))
        setData(allData)
        console.log(dataDb)
    }

    const deleteProduct = async (id) => {
        try {
          await deleteDoc(doc(DB, 'Products', id));
          getData();
        } catch (error) {
          console.error('Error deleting product:', error);
        }
      };
      
      useEffect(() => {
        getData();
      }, []);

    return (
        <div>
            <Navbar2 />
            <table class="table table-striped" id='tabel'>
                <thead>
                    <tr>
                        <th scope="col">Image</th>
                        <th scope="col" text-align='center'>Title</th>
                        <th scope="col">Author</th>
                        <th scope="col">Price</th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
            </table>
            {
                data.map(value => <div>
                    <table class="table table-striped" id='tabel'>
                        <tbody>
                            <tr >
                                <th scope="row"><img src={value.imgUrl} height='150vh' width='100px' alt="imagine"></img></th>
                                <th >{value.productTitle}</th>
                                <th >{value.productAuthor}</th>
                                <th >{value.productPrice}</th>
                                <th>
                                    <button className='button' id='edit' onClick={() => navigate(`/update/${value.id}`)}>Edit</button>
                                    
                                </th>
                                <th>
                                    
                                    <button className='button' id='delete' onClick={() => deleteProduct(value.id)}>Delete</button>
                                </th>

                            </tr>
                        </tbody>
                    </table>
                </div>)
            }

        </div>
    )
}

export default EditProducts
