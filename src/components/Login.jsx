import React, { useState } from 'react'
import "./Login.css"
import LoginImage from './images/login2.png'
import { BiEnvelope, BiLock } from "react-icons/bi";
import { Link, useNavigate } from "react-router-dom"
import { UserAuth } from '.././context/AuthContext';
import { collection, query, where, getDocs } from 'firebase/firestore';
import { DB } from "../firebase";

export const Login = () => {

    const { signIn } = UserAuth()
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [error, setError] = useState('')
    const navigate = useNavigate()

    const handleSubmit = async (e) => {
        e.preventDefault()
        setError('')
        try {
            const deletesCollection = collection(DB, 'deletedUsers')
            const q = query(deletesCollection, where('email', '==', email))
            const querySnapshot = await getDocs(q)

            if (querySnapshot.size === 0) {
                await signIn(email, password)
                navigate('/home')
            }
            else{
                alert('account doesnt exist')
            }
        } catch (e) {
            setError(e.message)
            console.log('user inexistent')
            alert("Date incorecte")
        }
    }

    return (
        <div class="login-container">
            <div class="login">
                <h1>Login</h1>
                <form onSubmit={handleSubmit}>
                    <div class="line">
                        <BiEnvelope className='icon-login' />
                        <input onChange={(e) => setEmail(e.target.value)} type="email" placeholder="Your Email" required />
                    </div>

                    <div class="line">
                        <BiLock className='icon-login' />
                        <input onChange={(e) => setPassword(e.target.value)} type="password" placeholder="Your Password" required />
                    </div>

                    <button className='buton-sign'> Sign in </button>

                    <div class="new_account">
                        Don't have an account? <Link to="/signup"> Sign Up </Link>
                    </div>
                </form>
            </div>

            <div class="login-image">
                <img src={LoginImage} alt="login" />
            </div>
        </div>
    )
}

