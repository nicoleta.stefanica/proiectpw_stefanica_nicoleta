import React, { useState } from 'react'
import { NavLink, useNavigate } from "react-router-dom"
import "./navbar.css"
import { UserAuth } from '../../context/AuthContext'

export const Navbar2 = () => {

    const [menuOpen, setMenu] = useState(false)

    const {logout} = UserAuth()
    const navigate = useNavigate()

    const handleLogOut = async () => {
        try {
            await logout()
            navigate('/')
            console.log('logged out')
        } catch (e) {
            console.log(e.message)
        }
    }


    return (
        <nav>
            <div className='logo'>
                <div>Bookish</div>
            </div>
            <div className='hamburger' onClick={() => setMenu(!menuOpen)}>
                <span></span>
                <span></span>
                <span></span>
            </div>
            <ul className={menuOpen ? "open" : ""}>
                <li>
                    <div><NavLink to="/home">Home</NavLink></div>
                </li>
                <li>
                    <NavLink to="/edit">Edit Products</NavLink>
                </li>
                <li>
                    <NavLink to="/add">Add Products</NavLink>
                </li>
                <li>
                    <NavLink to="/users">Users</NavLink>
                </li>
                <li>
                    <button className='buton-logout' onClick={handleLogOut} to="/">Log Out</button>
                </li>
            </ul>
        </nav>
    )
}
