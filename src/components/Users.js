import React, { useEffect, useState } from 'react'
import { Navbar2 } from './navbar/navbarAdmin'
import { DB } from '../firebase'
import { useNavigate } from "react-router-dom"
import { collection, getDocs } from "firebase/firestore";
import { deleteDoc, doc, addDoc } from "firebase/firestore";
import { getAuth } from "firebase/auth";
import { deleteUser } from "firebase/auth";
// import * as admin from 'firebase-admin'
// import { credentials } from '../../credentials.json';



export function Users() {

    const [data, setData] = useState([])
    const navigate = useNavigate()

    // admin.initializeApp({
    //     credential: admin.credential.cert(credentials),
    // })

    const getData = async () => {
        const valRef = collection(DB, 'users')
        const dataDb = await getDocs(valRef)
        const allData = dataDb.docs.map(val => ({ ...val.data(), id: val.id }))
        setData(allData)
        console.log(dataDb)
    }

    const handleClick = async () => {
        

        
        
        alert("Product added successfully")
    }

    const deleteUser = async (id, email) => {
        try {
            const valRef = collection(DB, 'deletedUsers')
            await addDoc(valRef, {email:email})
            await deleteDoc(doc(DB, 'users', id));
            getData();
        } catch (error) {
            console.error('Error deleting product:', error);
        }
    };

    useEffect(() => {
        getData();
    }, []);


    return (
        <div>
            <Navbar2 />
            <h1>Users</h1>
            <table class="table table-striped" id='tabel'>
                <thead>
                    <tr>
                        <th scope="col" text-align='center'>First Name</th>
                        <th scope="col">Last Name</th>
                        <th scope="col">email</th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
            </table>
            {
                data.map(value => <div>
                    <table class="table table-striped" id='tabel'>
                        <tbody>
                            <tr >
                                <th >{value.firstName}</th>
                                <th >{value.lastName}</th>
                                <th >{value.email}</th>
                                <th>
                                    <button className='button' id='edit' onClick={() => navigate(`/updateUser/${value.id}`)}>Edit</button>
                                </th>
                                <th>
                                    <button className='button' id='delete' onClick={() => deleteUser(value.id, value.email)}>Delete</button>
                                </th>

                            </tr>
                        </tbody>
                    </table>
                </div>)
            }

        </div>
    )

}

export default Users
