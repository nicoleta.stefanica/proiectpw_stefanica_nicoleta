import React, { useEffect, useState } from 'react';
import { Navbar } from './navbar/navbar';
import { DB } from '../firebase';
import { useNavigate } from "react-router-dom";
import { collection, query, where, getDocs } from 'firebase/firestore';
import { getAuth } from 'firebase/auth';
import './account.css'

export function Account() {
    const navigate = useNavigate();
    const [userData, setUserData] = useState([]); // Initialize as an empty array.

    function getUserID() {
        const auth = getAuth();
        const user = auth.currentUser;
        if (user !== null) {
            return user.email;
        }
    }

    const email = getUserID();

    const findUser = async () => {
        try {
            const usersRef = collection(DB, 'users');
            const usersQuery = query(usersRef, where('email', '==', email));
            const querySnapshot = await getDocs(usersQuery);

            if (!querySnapshot.empty) {
                const userDataArray = []; // Create an array to store user data.
                querySnapshot.forEach((doc) => {
                    userDataArray.push({ id: doc.id, ...doc.data() }); // Add document ID to the user data.
                });
                setUserData(userDataArray); // Set userData as an array.
            } else {
                setUserData([]); // Set userData as an empty array if no user is found.
            }
        } catch (error) {
            console.error('Error finding user by email:', error);
        }
    };

    useEffect(() => {
        findUser();
    }, []);

    return (
        <div>
            <Navbar />
            <div className="container-account"> {/* Add a container div */}
                {userData.map((value) => (
                    <div key={value.id} className="user-info"> {/* Apply CSS classes */}
                        <h1>Hello, {value.firstName} :)</h1>
                        <br></br>
                        <h1>Your data:</h1>
                        <div className='bold-your-data'>First Name:</div> <div> {value.firstName}</div>
                        <div className='bold-your-data'>Last Name: </div> <div> {value.lastName}</div>
                        <div className='bold-your-data'>Your email: </div><div>{value.email}</div>
                        <div className='bold-your-data'>Phone Number: </div><div>{value.phoneNumber}</div>
                        <div className='bold-your-data'>Address: </div><div>{value.adress}</div>
                        <div>
                            <button  id="edit" onClick={() => navigate(`/updateUser/${value.id}`)}>
                                Edit
                            </button>
                        </div>
                    </div>
                ))}
            </div>
        </div>
    );
}

export default Account;
