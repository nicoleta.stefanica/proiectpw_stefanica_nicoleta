import React from 'react'
import { Link } from "react-router-dom"
import "./Home.css"
import Winds from '../images/winds.jpg'
import { BiRightArrowAlt } from "react-icons/bi";
import { Navbar } from '../navbar/navbar'
import { Footer } from '../footer/Footer'
import { Carousel } from '../Carousel'
import { getAuth } from "firebase/auth";
import {Navbar2} from '../navbar/navbarAdmin'

export const Home = () => {

  function getUserID() {
    const auth = getAuth();
    const user = auth.currentUser;
    if (user !== null && user.email === 'admin@gmail.com') {
      return user.email;
    }
  }

  const user = getUserID()

  return (
    <div>

      {user ? <Navbar2 /> : <Navbar />}
      <section class="hero">
        <div class="hero-text">
          <h4>best prices</h4>
          <h1>new book releases</h1>


          <Link to='/shop' class="hero-btn"> Go to shop<BiRightArrowAlt /></Link>
        </div>
      </section>

      <section className="best-seller-text">
        <h1>bestsellers</h1>
        <Carousel />
      </section>

      <section className="preorder-section">
        <div className="preorder-text">
          <h2>NEW book release: <i>Winds of Winter</i> !</h2>
        </div>

        <div className="image-quote">
          <div className="preorder-image">
            <img src={Winds} alt="windsOfWinter" />
          </div>

          <div className="preorder-quote">
            <p>George RR Martin's long awaited book has finally arrived!
              Are you curious about what happened to your favorite characters from
              the <q>A Song of Ice and Fire</q> saga? Make
              sure you're among the first to read it, you can order it only on our website!</p>
          </div>

        </div>

      </section>

      <Footer />
    </div>
  )
}

