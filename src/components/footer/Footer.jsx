import React from 'react'
import './Footer.css'
import { BiLogoFacebookSquare, BiLogoInstagramAlt } from "react-icons/bi";

export function Footer() {
  return (
    <div className="footer">

        <div className="icons">
            <h4>Social</h4>
            <BiLogoFacebookSquare color="white" fontSize="1.5em" onClick={() => window.open('https://www.facebook.com/', '_blank')} className='icon'/>
            <BiLogoInstagramAlt color="white" fontSize="1.5em" onClick={() => window.open('www.instagram.com', '_blank')} className='icon'/>
        </div>

        <div className="contact">
            <h4>Contact</h4>
            <h2> 021 315 0653</h2>
            <h2> bookish@gmail.com</h2>
        </div>
    </div>
  )
}

export default Footer
