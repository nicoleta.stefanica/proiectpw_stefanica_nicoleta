import './App.css';

import { Route, Routes } from 'react-router-dom';
import { Home } from "./components/Home/Home"
import { Shop } from "./components/Shop/Shop"
import { Login } from "./components/Login"
import { Signup } from "./components/Signup"
import { EditProducts } from "./components/EditProducts/EditProducts"
import { AuthContextProvider } from './context/AuthContext';
import { Update } from './components/EditProducts/update'
import AddProducts from './components/EditProducts/AddProducts';
import {Cart} from './components/Shop/Cart'
import {Users} from './components/Users'
import { UserUpdate } from './components/UsersUpdate';
import {Account} from './components/account'

function App() {
  return (
    <div className='App'>
      <AuthContextProvider>
          <Routes>
            <Route exact path="/" element={<Login />} />
            <Route path="/signup" element={<Signup />} />
            <Route path="/home" element={<Home />} />
            <Route path="/shop" element={<Shop />} />
            <Route path="/add" element={<AddProducts />} />
            <Route path="/edit" element={<EditProducts />} />
            <Route path="/update/:id" element={<Update />} />
            <Route path="/cart" element={<Cart />} />
            <Route path="/users" element={<Users />} />
            <Route path="/updateUser/:id" element={<UserUpdate />} />
            <Route path="/account" element={<Account />} />
          </Routes>
      </AuthContextProvider>
    </div >
  );
}

export default App;
