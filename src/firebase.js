// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
import { getDatabase } from "firebase/database";
import { getStorage } from "firebase/storage";
import { getFirestore } from "firebase/firestore"



// Your web app's Firebase configuration
// const firebaseConfig = {
//   apiKey: "AIzaSyCchnDQ9sH7tp45GEFZmA8kX_Ds7HGNbIA",
//   authDomain: "proiect-pw-b1cee.firebaseapp.com",
//   projectId: "proiect-pw-b1cee",
//   storageBucket: "proiect-pw-b1cee.appspot.com",
//   messagingSenderId: "362392384193",
//   appId: "1:362392384193:web:4ef1b6b87185e670a0da40"
// };
//al 2lea data base - proiectpw
const firebaseConfig = {
  apiKey: "AIzaSyAEgmF65vBsLsNEmFc5oVq798r7DC48qI0",
  authDomain: "proiectpw-ce5b6.firebaseapp.com",
  projectId: "proiectpw-ce5b6",
  storageBucket: "proiectpw-ce5b6.appspot.com",
  messagingSenderId: "472116433213",
  appId: "1:472116433213:web:8b3de4a94541685ac6f554"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

export const auth = getAuth(app);
export const db = getDatabase(app);
export const database = getDatabase(app);
export const storage = getStorage(app);
export const imgDB = getStorage(app)
export const txtDB = getFirestore(app)
export const DB = getFirestore(app)
// export const { admin } = require('firebase-admin/app');
export default app