**Cerințe și specificații**

**Cerințe:**
-	Aplicație pentru o librărie online, pe care utilizatorii o pot accesa prin intermediul unui cont;
-	Utilizatorii sunt de 2 tipuri: administrator și client;
-	Administratorul poate adăuga produse, poate edita sau șterge pe cele deja existente;
-	Utilizatorul poate adăuga produse într-un coș, care vor reprezenta comanda acestuia.

**Specificații:**
-	Aplicația se deschide cu pagina de Log In, unde utilizatorii trebuie să se conecteze la contul lor. Dacă datele sunt incorecte sau contul nu există, se va afișa un mesaj de eroare. Apoi vor fi redirecționați spre pagina de Home, un cadru introductiv în aplicație. Dacă nu au deja cont, poate fi accesată pagina de Sign Up unde își pot crea unul;
-	Administratorul are acces la secțiunile de „Add Product”, unde poate adăuga noi produse și „Edit Products”, unde va apărea un tabel cu toate produsele existente și 2 butoane în dreptul fiecărui produs. Delete va șterge produsul din baza de date, iar Edit va deschide o pagină în care câmpurile produsului sunt completate cu valorile actuale, dar care pot fi înlocuite. Acțiunea se definitivează prin apăsarea butonului de Update, iar dacă nu se mai doresc schimbări, ieșirea se face cu butonul Go Back. Administartorul poate accesa și secțiunea de „Users”, unde sunt afișați toți utilizatorii, putând să le modifice datele sau să îi șteargă;
-	 Clientul are acces la secțiunea „Shop”, unde sunt afișate produsele disponibile și la coșul său de cumpărături, unde vor fi afișate produsele selectate și prețul total. Produsele din coș pot fi șterse sau li se poate incrementa/decrementa cantitatea. User-ul mai are acces si la pagina de „Account”, unde îi sunt afișate datele, dar pe care le poate modifica
-	Ambii au acces la pagina de „Home”.
Tehnologii utilizate:  HTML, CSS, JavaScript, React, Firebase, Bootstrap.

